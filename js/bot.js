(function() {
	var texts = [];
	var mood = 'all';
	var botElement = document.getElementById('bot');

	var apologies = [
		'Pardon…',
		'Pardon !'
	];

	var reactions = [
		'Grmpf…',
		'Grmpf !'
	];

	var onomatopoeiaMouse = [
		'CLIC'
	];

	var onomatopoeiaKeyboard = [
		'TAC'
	];

	var textSources = [
		{
			sourceType: 'dom',
			content: 'Machins mes couilles…',
			frequency: 2,
			reactions: apologies,
			reactionsFrequency: 0.2,
			moods: ['blase', 'angry']
		},
		{
			sourceType: 'dom',
			content: 'Machins mes couilles !',
			frequency: 2,
			reactions: apologies,
			moods: ['angry']
		},
		{
			sourceType: 'dom',
			content: 'Mais c\'est d\'la merde !',
			frequency: 2,
			reactions: apologies,
			reactionsFrequency: 0.2,
			moods: ['angry']
		},
		{
			sourceType: 'dom',
			content: 'C\'est ce qu\'on appelle communément un bon gros étron !',
			frequency: 1,
			reactions: apologies,
			reactionsFrequency: 0.2,
			moods: ['angry']
		},
		{
			sourceType: 'dom',
			content: 'Espèce d\'étron sur pattes !',
			frequency: 1,
			reactions: apologies,
			reactionsFrequency: 0.2,
			moods: ['angry']
		},
		{
			sourceType: 'dom',
			content: '♫ Ta-ta yo-yo ! ♫',
			frequency: 1,
			moods: ['angry', 'blase', 'happy']
		},
		{
			sourceType: 'dom',
			content: 'Fais chier, salope !',
			frequency: 1,
			reactions: apologies,
			reactionsFrequency: 0.2,
			moods: ['angry']
		},
		{
			sourceType: 'dom',
			content: 'Mais non on s\'en branle connard !',
			frequency: 1,
			reactions: apologies,
			reactionsFrequency: 0.2,
			moods: ['angry']
		},
		{
			sourceType: 'dom',
			content: 'Ça commence à m\'péter les couilles velues c\'truc là !',
			frequency: 1,
			reactions: apologies,
			reactionsFrequency: 0.2,
			moods: ['angry']
		},
		{
			sourceType: 'others',
			content: '♫ Laisse pas traîner ton fils ! ♫',
			frequency: 3,
			reactions: reactions,
			reactionsFrequency: 1,
			moods: ['blase']
		},
		{
			sourceType: 'others',
			content: '♫ Où t\'es, papaoutai ? Où t\'es ? ♫',
			frequency: 3,
			reactions: reactions,
			reactionsFrequency: 1,
			moods: ['blase']
		},
		{
			sourceType: 'dom',
			content: 'Tu joues à l’hippocampe magique ?',
			frequency: 1,
			moods: ['happy']
		},
		{
			sourceType: 'dom',
			content: 'Pas de quoi casser trois pattes à un canard…',
			frequency: 1,
			moods: ['blase']
		},
		{
			sourceType: 'dom',
			content: 'Quand l\'huitre lache une perle, le mérou pète.',
			frequency: 1,
			moods: ['happy', 'blase']
		},
		{
			sourceType: 'dom',
			content: 'J\'y jetterais pas mon slip gratuitement…',
			frequency: 1,
			moods: ['happy']
		},
		{
			sourceType: 'dom',
			content: 'MOUAHAHAHAHA !',
			frequency: 2,
			moods: ['angry', 'blase', 'happy']
		},
		{
			sourceType: 'dom',
			content: 'Tu te rends compte que tu te domise ?',
			frequency: 2,
			reactions: ['Et ça te fait pas… peur ?'],
			reactionsFrequency: 1,
			moods: ['happy']
		}
	];

	compileTexts();
	showText();
	showOnomatopoeiaMouse();
	showOnomatopoeiaKeyboard();

	/**
	 * Compile texts for the current mood.
	 */
	function compileTexts() {
		texts = [];
		for (var i = 0; i < textSources.length; i++) {
			var text = textSources[i];
			if (mood !== 'all' && text.moods.indexOf(mood) === -1) {
				continue;
			}

			var frequency = text.frequency || 1;
			for (var j = 0; j < frequency; j++) {
				texts.push(text);
			}
		}
	}

	/**
	 * @param {string[]} texts
	 * @returns {string}
	 */
	function getRandom(texts) {
		return texts[Math.floor(Math.random() * texts.length)];
	}

	/**
	 *
	 * @param {string} text
	 * @param {string} className
	 * @param {number} x
	 * @param {number} y
	 * @param {number} w
	 * @param {number} h
	 * @returns {number}
	 */
	function doShow(text, className, x, y, w, h) {
		var bubble = botElement.querySelector('.' + className);
		var canvas = botElement.querySelector('.' + className + ' .bubble-border');
		var div = botElement.querySelector('.' + className + ' .bubble-text');
		if (div.firstChild) {
			div.removeChild(div.firstChild);
		}
		div.appendChild(document.createTextNode(text));
		div.setAttribute('class', 'bubble-text' + ((text.length < 50) ? '' : ' bubble-text-long'));

		var ctx = canvas.getContext('2d');
		drawBubble(ctx, x, y, w, h, 7);

		bubble.style.visibility = 'visible';

		var delay = 1000 + 120 * text.length;
		setTimeout(function() { bubble.style.visibility = 'hidden'; }, delay);
		return delay;
	}

	function showText() {
		var text = getRandom(texts);
		if (text.sourceType === 'dom') {
			var delay = doShow(text.content, 'bubble-main', 7, 1, 192, 68);
			if (text.reactions && (Math.random() <= text.reactionsFrequency)) {
				setTimeout(function() { showReaction(text.reactions) }, delay);
				delay += 5000;
			}
		}
		else if (text.sourceType === 'others') {
			delay = doShow(text.content, 'bubble-other', 7, 1, 192, 68);
			if (text.reactions && (Math.random() <= text.reactionsFrequency)) {
				setTimeout(function() { showReaction(text.reactions) }, (delay - 1000));
				delay += 5000;
			}
		}

		delay += 5000 + Math.floor(1000 * 10 * Math.random());
		setTimeout(function() { showText(); }, delay);
	}

	function showReaction(reactions) {
		var text = getRandom(reactions);
		if (text.length < 8) {
			doShow(text, 'bubble-reaction', 7, 1, 92, 23);
		}
		else {
			doShow(text, 'bubble-reaction-long', 7, 1, 192, 68);
		}
	}

	function showOnomatopoeiaMouse() {
		var text = getRandom(onomatopoeiaMouse);
		var delay = doShow(text, 'bubble-onomatopoeia-1', 7, 1, 62, 23);

		delay += Math.floor(1000 * 6 * Math.random());
		setTimeout(function() { showOnomatopoeiaMouse(); }, delay);
	}

	function showOnomatopoeiaKeyboard() {
		var text = getRandom(onomatopoeiaKeyboard);
		var delay = doShow(text, 'bubble-onomatopoeia-2', 7, 1, 62, 23);

		delay += Math.floor(1000 * 2 * Math.random());
		setTimeout(function() { showOnomatopoeiaKeyboard(); }, delay);
	}

	function drawBubble(ctx, x, y, w, h, radius) {
		var r = x + w;
		var b = y + h;
		ctx.beginPath();
		ctx.strokeStyle = 'black';
		ctx.fillStyle = 'white';
		ctx.lineWidth = '2';
		ctx.moveTo(x + radius, y);
		ctx.lineTo(r - radius, y);
		ctx.quadraticCurveTo(r, y, r, y + radius);
		ctx.lineTo(r, y + h - radius);
		ctx.quadraticCurveTo(r, b, r - radius, b);
		ctx.lineTo(x + radius, b);
		ctx.quadraticCurveTo(x, b, x, b - radius);
		ctx.lineTo(x - 7, b - radius - 3);
		ctx.lineTo(x, b - radius - 3);
		ctx.lineTo(x, y + radius);
		ctx.quadraticCurveTo(x, y, x + radius, y);
		ctx.stroke();
		ctx.fill();
	}

	document.DomBot = {
		setMood: function(newMood) {
			document.querySelector('#mood .active').className = null;
			document.getElementById('mood-' + newMood).className = 'active';
			mood = newMood;
			compileTexts();
		}
	}
})();